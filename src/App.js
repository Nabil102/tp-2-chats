import React from "react";
import "./App.css";
import Title from "./component/header";
import ScreenCahts from "./component/ScreenCahts"

class App extends React.Component {
  render() {
    return (
      <>
        <div className="title container">
          <Title />
        </div>
        <div className="container">
          <ScreenCahts/>
        </div>
      </>
    );
  }
}
export default App;
