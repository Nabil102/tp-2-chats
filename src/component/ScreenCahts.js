import ReactPaginate from "react-paginate";
import React, { useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";

const ScreenChats = () => {
  const [catsData, setData] = useState([]);
  const [loading, setLoading] = useState(false);

  async function fetchData() {
    const res = await fetch(
      "https://api.thecatapi.com/v1/images/search?page=1&order=Desc&limit=8"
    );
    const data = await res.json();
    setLoading(true);
    setData(data);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const fetchCats = async (currentPage) => {
    const res = await fetch(
      // eslint-disable-next-line no-template-curly-in-string
      "https://api.thecatapi.com/v1/images/search?page=${currentPage}&order=Desc&limit=8"
    );
    const data = await res.json();
    return data;
  };

  const handlePageClick = async (data) => {
    console.log(data.selected);
    let currentPage = data.selected + 1;
    const catsApiServer = await fetchCats(currentPage);
    setData(catsApiServer);
  };

  return (
    <>
      <>
        <>
          {loading ? (
            <div>
              <div className="row">
                <div className="col">
                  {catsData.map((cat) => (
                    // eslint-disable-next-line jsx-a11y/alt-text
                    <img src={cat.url} />
                  ))}
                </div>
              </div>
            </div>
          ) : (
            <div className="position">
              <Spinner  animation="border" role="status">
                <span className="visually-hidden ">Loading...</span>
              </Spinner>
            </div>
          )}
        </>
        <div>
          <ReactPaginate
            previousLabel={"Précédent"}
            nextLabel={"Suivant"}
            pageCount={30}
            containerClassName={"pagination justify-content-center mt-5"}
            onPageChange={handlePageClick}
            pageClassName={"page-item"}
            pageLinkClassName={"page-link"}
            previousClassName={"page-item"}
            previousLinkClassName={"page-link"}
            nextClassName={"page-item"}
            nextLinkClassName={"page-link"}
            breakClassName={"page-item"}
            breakLinkClassName={"page-link"}
            activeClassName={"active"}
          />
        </div>
      </>
    </>
  );
};

export default ScreenChats;
